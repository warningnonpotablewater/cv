---
title: "Ruby Klover's CV"
description: "A software developer."
---

# Ruby Klover

## Contact

* <span class="caption">Email:</span> rubyklover@outlook.com
* <span class="caption">Phone:</span> Available upon request

## Skills

### Programming languages

* JavaScript
* Python

### Technologies

* HTML, CSS, basic Web accessibility
* Git, basic Linux administration

### Natural languages

* English (C1 Advanced)
* Russian (native)

## Projects

### [www.bruh.ltd][www] (Hugo)

[www]: https://www.bruh.ltd/

Y2K-styled personal website

### [webbotron.bruh.ltd][wt] (TypeScript + Svelte)

[wt]: https://webbotron.bruh.ltd/

Baby name picker app for nerds

### [bm1.bruh.ltd][bm1] (Rust)

[bm1]: https://bm1.bruh.ltd/

Work in progress RISC-V based fantasy computer

## Education

### Computer Programmer

{{< subtitle "2018 to 2022" >}}

Secondary vocational
